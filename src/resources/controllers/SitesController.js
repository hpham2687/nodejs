const productModel = require('./../models/ProductModel');

class SiteController {
  //get home
  async index(req, res) {
    try {
      let allProducts = await productModel.find({});
      console.log(allProducts);
      allProducts = allProducts.map((product) => product.toObject());
      res.render('home', { allProducts });
    } catch (err) {
      console.log(err);
    }

    //res.render('home');
  }

  // search
  search(req, res) {
    res.send('search');
  }
}

module.exports = new SiteController();

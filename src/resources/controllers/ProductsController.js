const productModel = require('./../models/ProductModel');

class ProductsController {
  //get /products/:slug

  async show(req, res) {
    try {
      let product = await productModel.findOne({ slug: req.params.slug });
      if (!product) res.json('no product');
      else res.render('Product/productDetail', product);
    } catch (err) {
      console.log(err);
    }
  }

  async create(req, res) {
    res.render('Product/createPage');
  }

  async edit(req, res) {
    let product = await productModel.findOne({ slug: req.params.slug });

    res.render('Product/editPage', product);
  }
  async update(req, res) {
    await productModel.updateOne({ slug: req.params.slug }, req.body);
    res.redirect('/');
  }

  async store(req, res) {
    let { name, price, tags, images } = req.body;
    if (!name || !price || !tags || !images) console.log('field emtpy!');
    else {
      var newProduct = new productModel({ ...req.body, slug: 'new-product' });
      try {
        await newProduct.save();
        console.log(name + ' saved to bookstore collection.');
        res.redirect('/');
      } catch (error) {
        return console.error(error);
      }
    }
    //  res.render('Product/createPage')
  }
}

module.exports = new ProductsController();

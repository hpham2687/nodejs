const express = require('express');
const router = express.Router();

var newsController = require('./../controllers/NewsController');

router.route('/').get(newsController.index);
router.route('/:slug').get(newsController.show);

module.exports = router;

const newsRouter = require('./../routers/NewsRouter');
const sitesRouter = require('./SitesRouter');
const productsRouter = require('./ProductsRouter');

function route(app) {
  app.use('/news', newsRouter);
  app.use('/products', productsRouter);

  app.use('/', sitesRouter);
}
module.exports = route;

const express = require('express');
const router = express.Router();

var productsController = require('../controllers/ProductsController');

router
  .route('/create')
  .get(productsController.create)
  .post(productsController.store);
router.route('/:slug/edit').get(productsController.edit);
router.route('/:slug/update').post(productsController.update);

router.route('/:slug').get(productsController.show);

module.exports = router;

const express = require('express');
const router = express.Router();

var siteController = require('../controllers/SitesController');

router.route('/search').get(siteController.search);
router.route('/').get(siteController.index);

module.exports = router;

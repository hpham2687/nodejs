const mongoose = require('mongoose');
const URLSlugs = require('mongoose-url-slugs');

const productSchema = new mongoose.Schema({
  name: { type: String, default: 'default name' },
  price: { type: Number },
  tags: { type: String },
  images: {
    type: String,
    default:
      'https://res.cloudinary.com/redq-inc/image/upload/c_fit,q_auto:best,w_300/v1589614568/pickbazar/grocery/Yellow_Limes_y0lbyo.jpg',
  },
});
productSchema.plugin(URLSlugs('name'));

const Product = mongoose.model('Product', productSchema);

module.exports = Product;

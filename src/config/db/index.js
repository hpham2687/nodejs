const mongoose = require('mongoose');
async function connect() {
  try {
    mongoose.connect('mongodb://localhost:27017/grocery_shop', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log('db connected');
  } catch (err) {
    console.log('db rr', err);
  }
}
module.exports = { connect };

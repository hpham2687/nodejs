const express = require('express');
const morgan = require('morgan');
const handlebars = require('express-handlebars');
const path = require('path');
const db = require('./config/db');
var bodyParser = require('body-parser');
const PORT = 3000;
const route = require('./resources/routers');
const app = express();

db.connect();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(morgan('tiny'));
app.use(express.static(path.join(__dirname, 'public')));
//template engine

app.engine('.hbs', handlebars({ extname: '.hbs' }));
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, 'resources', 'views'));

route(app);

app.listen(PORT, () => {
  console.log('Listening on port ', PORT);
});
